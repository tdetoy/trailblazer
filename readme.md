#Welcome!

This is the first pass at the encapsulated set of files for the larger TrailBlazer project. Please check out the info below:

**Project Structure**

This is largely set up to be droppable into another project. There is some information that will be necessary for operation though.

*Debug* - Debug contains all of the visual debug help that I used on this project, including the CanvasLayer for drawing pheromone trails, etc, including the test scene.
Technically, nothing in here is necessary for operation, but please note that there are a couple of debug signals in both Iacus and Worker Ant scenes:

-`new_ant`, and
-`processing_ant`

If you do not need any additional debug, you can safely remove those signals and their connections.

*Tools* - Tools contains all of the additional helper items that ensure these behaviors operate smoothly. `AntPathPosition.tscn` and `TrailConnection.tscn` should be self-contained,
and there should be no need to place them into a scene manually. `TrailConnection` does have some configurable values related to easing though, which can be useful to check out.

`AntContainers.tscn` and `PheromoneTrailContainer.tscn` *are* necessary for Iacus and ants to operate properly. They are meant to exist in the scene as static transforms to avoid accidentally
inheriting dynamic transforms from parent. Iacus needs to be aware of the location of these nodes in a scene, and they have a couple of exported NodePath variables that will need to be initialized
once you have them in the scene. It would also be useful to keep them in relatively stable positions in the SceneTree as well - worker ants freely move between the children in `AntContainers`
depending on their current behavior.

*Camera* is fairly self-explanatory, and you shouldn't need to do much. It's already a child node of Iacus and can either rotate with them or keep a fixed rotation. Zoom is also possible.

*Iacus* - As you might expect, the player character is here. Drop them in the scene, and give them knowledge of the containers mentioned above. Iacus also comes with some configurable settings for
movement and follow - feel free to configure these to your heart's content. Currently, Iacus personally handles worker ant spawning. This is likely too much coupling, but it shouldn't be
too much work to separate the two. 

*Food* - Food is currently pretty empty, as it only holds the Blueberry and Pebble items. This folder structure exists for the future when there may need to be a system in place to place food as necessary. The Blueberry has a spherical area around it that detects the presence of Iacus, and notifies their code so that the next pheromone waypoint can snap to it. Pebbles are held in the worker ants' mouths when they harvest from the Blueberry. Blueberries are used up after a time.

*Replete* - Here is the (currently very simple) Replete information. As it stands right now, Repletes act more like a pheromone trail waypoint than they do a mobile entity (like worker ants). This is mainly for simplicity of implementation now, but this can be changed in the future. Repletes are manually placed on the field currently, and are stationary. When a worker ant arrives holding a pebble, they will give the pebble to the Replete and the Replete's abdomen will expand.

*Worker* - This is where all of the data for the worker ant is stored. Worker ants are fairly unaware of the world around them right now, they're mainly provided the information they need as they
need it. One of the biggest things that can be used to configure an individual ant is the `UnitSettings` class, contained within the worker ant script. It contains a number of useful items to
configure worker ants as needed:

- `speed`: the speed of the ant, unsurprisingly. There's also a `min_speed` that is currently unimplemented.
- `deceleration`: used to bring the ant to a halt during its idle behavior
- `perception_radius` and `avoidance_radius`: these represent the ant's ability to see and how close they let other ants get, respectively. Currently, the former doesn't do much but it exists for future use.
- `max_steer_force`: max vector magnitude when determining steering
- `follow_distance`: the follow distance when in follow behavior
- `appearance_scale`: a multiplier for the ant's scale, uniform in all directions.
- `avoid_coll_weight`: how much the ant prioritizes trying to get out of the way of other ants during idle behavior. 
- `appearance_scale`: as the name suggests, you can scale the size of a worker ant with this value.

There are default values for the above, and it's simple to reuse an instance of `UnitSettings` as a template for all other ants. Whenever an ant is instanced, it needs the `ant_container` that Iacus has,
and it's recommended to give it a `UnitSettings` (though not necessary, unlike the container information).

The `project.godot` file may also be helpful. There are a number of additional keybindings added right now for testing and usage:

- `move_forward`, `move_backward`, `turn_left`, `turn_right`: movement keys for Iacus <- *UPDATED:* these keys are relative to the orientation of the camera, rather than absolute tank-control style
- `summon_ant` : will summon one worker ant to the default origin
- `lay_trail` : hold this key down while moving to lay pheromone trails
- `dismiss_followers` : currently, this releases all ants following Iacus and assigns them to the last laid pheromone trail, if possible. If there's no trail, the ants are not dismissed
- `rotate_camera_left` and `rotate_camera_right` : these will orbit the camera around Iacus at a fixed rate

Also, there is some naming to the first three physics layers. This is also laid out in the comments for Iacus and the worker ant script, but the first three layers are currently used.

Layer 1 is for Iacus and worker ant bodies. The environment also needs this layer so Iacus does not fall through the world.
Layer 2 is for the perception radii that ants have. They operate with a mask on layer 1 so that they do not detect each other
Layer 3 is the secondary layer for the environment. Ants rely on layer 3 to get a clear picture of the terrain so they can place themselves on it sanely.

The main takeaway of the above is that all solid terrain should be on layers 1 and 3, and mask for 1.

As this is just a first pass, I am absolutely open to more changes. Even in writing this document, I realized there is room for improvement in what currently exists, and I'm sure there's more that you
recognize that could be done as well. Please don't hesitate to let me know!
