extends Node2D

onready var conv = get_parent().get_node("Spatial")

func _draw():
	var trails = get_tree().get_root().get_node("Spatial/PheromoneTrail").get_children()
	for trail in trails:
		var points_2d = []
		for point in trail.curve.get_baked_points():
			point = conv.to_global(point) + trail.transform.origin
			points_2d.append(get_tree().get_root().get_camera().unproject_position(point))
		if (points_2d.size() >= 2):
			draw_polyline(PoolVector2Array(points_2d), Color.blue, 3.0)
