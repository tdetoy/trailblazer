extends CanvasLayer

var ants = 0
var processing_ants = 0

func _process(_delta):
	$VBoxContainer/FPS/Value.text = str(Engine.get_frames_per_second())

func _update_ants(value):
	ants += value
	$VBoxContainer/NumAnts/Value.text = str(ants)

func _update_processing(value):
	processing_ants += value
	$VBoxContainer/NumProcessingAnts/Value.text = str(processing_ants)
