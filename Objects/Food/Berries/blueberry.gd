extends Spatial

var blue1 = preload("res://Objects/Food/Berries/Raw Assets/blueberry1.tscn")
var blue2 = preload("res://Objects/Food/Berries/Raw Assets/blueberry2.tscn")
var blue3 = preload("res://Objects/Food/Berries/Raw Assets/blueberry3.tscn")
var blue4 = preload("res://Objects/Food/Berries/Raw Assets/blueberry4.tscn")

export var detection_radius := 1.5
export var possible_actions = ["Gather"]

onready var appearance = $BlueberryModel

var blueberry_left = 12

func _on_Area_area_entered(area):
	if (area.is_in_group("WorkerAnt")):
		pass

func _on_Area_body_entered(body):
	if (body.name == "Iacus"):
		body.notify_nearby_point(self)

func _on_Area_body_exited(body):
	if (body.name == "Iacus"):
		body.notify_leaving_nearby_point(self)

func decrement_uses():
	blueberry_left -= 1
	
	match blueberry_left:
		9:
			var current_appearance = appearance.get_child(0)
			appearance.remove_child(current_appearance)
			current_appearance.queue_free()
			current_appearance = blue2.instance()
			appearance.add_child(current_appearance)

		6:
			var current_appearance = appearance.get_child(0)
			appearance.remove_child(current_appearance)
			current_appearance.queue_free()
			current_appearance = blue3.instance()
			appearance.add_child(current_appearance)
		3:
			var current_appearance = appearance.get_child(0)
			appearance.remove_child(current_appearance)
			current_appearance.queue_free()
			current_appearance = blue4.instance()
			appearance.add_child(current_appearance)
			
		0:
			queue_free()
