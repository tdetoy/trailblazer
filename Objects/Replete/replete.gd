extends Spatial


export var detection_radius := 5.0
export var possible_actions = ["Provide"]

var inflate = 0.0

func _process(delta):
	var set_val = lerp($"Replete-ant/replete_Ant_Arm/Skeleton/Sphere".get("blend_shapes/Inflate"), inflate, 0.8)
	$"Replete-ant/replete_Ant_Arm/Skeleton/Sphere".set("blend_shapes/Inflate", set_val)

func _on_DetectionArea_area_entered(area):
	if (area.is_in_group("WorkerAnt")):
		pass

func _on_DetectionArea_body_entered(body):
	if (body.name == "Iacus"):
		body.notify_nearby_point(self)

func _on_DetectionArea_body_exited(body):
	if (body.name == "Iacus"):
		body.notify_leaving_nearby_point(self)

func receive_food():
	inflate = min(inflate + 0.5, 1.0)
