extends KinematicBody

##############################################
# TO KNOW FOR IACUS:                         #
# ant_container and pheromone_trail_location #
# must be filled with the appropriate paths  #
# or ants will not properly spawn            #
# Iacus operates on physics layer 1, so keep #
# environment layer and mask 1 checked       #
# (basically, 1 and 3 are necessary for      #
# everything right now, in the environment.) #
##############################################

# note that also you can configure trail easing
# parameters (check trail_connection.gd for  
# more information)

#debug signals
signal new_ant(value)

#operation signals
signal summon_ant
signal rot_update(value)
signal new_trail(position)
signal update_trail(position, object)
signal add_pathers(line)

var trailconnection_template = preload("res://Objects/Tools/TrailConnection.tscn")

export var gravity = -0.9
export var trail_separation_dist = 20.0
export var follow_distance := 20.0
export var speed := 20.0
export var rotation_speed := 0.05
export var max_steer_force := 0.9
export (NodePath) var ant_container_location
export (NodePath) var pheromone_trail_location

onready var prev_y_rot = rotation.y
onready var current_vel := Vector3.ZERO

var laying_trail := false
var following_line := [self]
var previous_trail_location : Vector3
var follower_ready := true
var nearest_trail_landmark

func _ready():
	#prime Iacus's animation
	$CH_Shepherd/AnimationPlayer.play("Walk_Cycle")
	$CH_Shepherd/AnimationPlayer.advance(0.2)
	yield(get_tree(), "idle_frame")
	$CH_Shepherd/AnimationPlayer.stop()
	
	#signal hookups
	prints("connecting 'summon_ant':",
	connect("summon_ant", get_node(ant_container_location), "_on_summon_ant"))
	
	prints("connecting 'new_trail':",
	connect("new_trail", get_node(pheromone_trail_location), "_on_new_trail"))
	
	prints("connecting 'update_trail':",
	connect("update_trail", get_node(pheromone_trail_location), "_on_update_trail"))
	
	prints("connecting 'add_pathers':",
	connect("add_pathers", get_node(pheromone_trail_location), "_on_add_pather"))
	
	prints("connecting 'report_remaining_pathers':",
	get_node(pheromone_trail_location).connect("report_remaining_pathers", self, "_on_report_remaining_pathers"))
	get_node(pheromone_trail_location).distance = trail_separation_dist
	
	prints("connecting 'rot_update':",
	connect("rot_update", $CameraPivot, "_compensate_rotation"))
	
#	prints("connecting 'new_ant':", connect("new_ant", get_tree().get_root().get_node("Spatial/DebugInfo"), "_update_ants"))

func _physics_process(_delta):
	var move = Vector2.ZERO
	move.y = Input.get_action_strength("move_backward") - Input.get_action_strength("move_forward")
	move.x = Input.get_action_strength("move_right") - Input.get_action_strength("move_left")
	var correction = Vector3.UP * gravity
	
	if (is_on_floor()):
		correction = Vector3.UP - get_floor_normal()
		correction *= gravity
	if(move != Vector2.ZERO):
		$CH_Shepherd/AnimationPlayer.play("Walk_Cycle")
	else:
		$CH_Shepherd/AnimationPlayer.stop()
	
	if (laying_trail):
		if (transform.origin.distance_to(previous_trail_location) > trail_separation_dist):
			emit_signal("update_trail", transform.origin if !nearest_trail_landmark else Vector3(nearest_trail_landmark.transform.origin.x, 1.0, nearest_trail_landmark.transform.origin.z))
			previous_trail_location = transform.origin
	
	var target = Vector3(move.x, 0.0, move.y).rotated(Vector3.UP, $CameraPivot.rotation.y + (PI / 4))
	
	target = steer_towards(target, current_vel)
	current_vel += target

	#vector2's angle_to function returns negative angles, but not the smallest
	#vector3's angle_to function returns the smallest angle, but no negatives
	#get both, and rotate in the correct direction using the v3's angle,
	#based on the sign of the v2's angle
	#then, the model's transform needs to align (as it does not automatically)
	if (move != Vector2.ZERO):
		var facing = Vector2(transform.basis.z.x, transform.basis.z.z)
		var desired_facing = Vector2(current_vel.x, current_vel.z)
		var d_angle = facing.angle_to(desired_facing)
		var angle = transform.basis.z.angle_to(current_vel)
		rotate_y(-angle if d_angle > 0 else angle)
		$CH_Shepherd.transform.basis = transform.basis
	
	emit_signal("rot_update", prev_y_rot - rotation.y)
	prev_y_rot = rotation.y
	var final_vel = current_vel
	final_vel.y = correction.y
	var _drop = move_and_slide(final_vel.rotated(Vector3.UP, rotation.y), Vector3.UP)

func _unhandled_input(event):
	if (event.is_action_pressed("summon_ant")):
		emit_signal("summon_ant")
	
	if (event.is_action_pressed("dismiss_followers")):
		follower_ready = false
		emit_signal("add_pathers", following_line)
	
	if (event.is_action_pressed("lay_trail")):
		if (nearest_trail_landmark):
			emit_signal("new_trail", nearest_trail_landmark.transform.origin, nearest_trail_landmark)
		else:
			emit_signal("new_trail", transform.origin)
		previous_trail_location = transform.origin if !nearest_trail_landmark else Vector3(nearest_trail_landmark.transform.origin.x, 1.0, nearest_trail_landmark.transform.origin.z)
		laying_trail = true
	
	if (event.is_action_released("lay_trail")):
		if (nearest_trail_landmark):
			emit_signal("update_trail", Vector3(nearest_trail_landmark.transform.origin.x, 1.0, nearest_trail_landmark.transform.origin.z), nearest_trail_landmark)
		laying_trail = false

func create_connection(connection):
	add_child(connection)
	connection.set_connection_length(follow_distance)
	prints("connecting 'rot_update", connect("rot_update", connection, "_on_rot_update"))

func add_to_line(obj):
	var new_connection = trailconnection_template.instance()
	following_line.back().create_connection(new_connection)
	following_line.append(obj)
	return new_connection

func _on_report_remaining_pathers(items : Array):
	following_line = items
	follower_ready = true

func steer_towards(target : Vector3, current : Vector3) -> Vector3:
	var final = target.normalized() * speed - current
	if (final.length() > max_steer_force):
		final = final.normalized() * max_steer_force
	return final

func notify_nearby_point(object):
	prints("found", object.name)
	nearest_trail_landmark = object

func notify_leaving_nearby_point(object):
	if (nearest_trail_landmark == object):
		nearest_trail_landmark = null
