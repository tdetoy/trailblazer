extends Position3D

# This is an object that gets placed between each
# ant in a trail - think a train car hitch.
# follow distance is configurable (option stored within
# ant settings, or manual with Iacus)
# as is easing information. 
# easing_delay is time in seconds before easing kicks in
# easing_speed is rotation speed in radians to return to normal
# setting easing_delay to 0 will eliminate easing

onready var timer = $Timer
onready var tween = $Tween

var easing_speed := 1.0
var easing_delay := 0.0

func _ready():
	set_process(false)

func _process(_delta):
#	debug drawing, kept just in case it's useful
#	$ImmediateGeometry.clear()
#	$ImmediateGeometry.begin(Mesh.PRIMITIVE_POINTS)
#	$ImmediateGeometry.add_vertex(Vector3.ZERO)
#	$ImmediateGeometry.add_vertex($BackConnection.transform.origin)
#	$ImmediateGeometry.end()
	pass

func get_back_connection() -> Vector3:
	return to_global($BackConnection.transform.origin)

func set_connection_length(value : float) -> void:
	$BackConnection.transform.origin = Vector3(0, 0, value)

func set_easing_responsiveness(speed := 1.0, delay := 1.0) -> void:
	easing_speed = speed
	easing_delay = delay

func _on_rot_update(value : float) -> void:
	if(is_equal_approx(value, 0.0) || easing_delay == 0.0):
		return
	
	rotate_y(value)
	if (tween.is_active()):
		tween.stop_all()
	if (!timer.is_stopped()):
		timer.stop()
	timer.start(easing_delay)

func _on_Timer_timeout() -> void:
	var rotation_time = abs(rotation.y) / easing_speed
	tween.interpolate_property(self, "rotation:y", rotation.y, 0.0, rotation_time, Tween.TRANS_CUBIC, Tween.EASE_OUT_IN)
	tween.start()
