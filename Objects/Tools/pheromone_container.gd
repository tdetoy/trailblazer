extends Spatial

signal report_remaining_pathers(trail)

#var pathfollow_template = preload("res://Objects/Tools/AntPathPosition.tscn")
var pathfollow_node = preload("res://Objects/Tools/PheromoneTrail.tscn")

var current_path #will hold the pathfollow_node above
var current_trail : Array
var trails : Array
var distance : float

func _on_new_trail(position : Vector3, object = null):
	current_path = pathfollow_node.instance()
	add_child(current_path)
	current_path.transform.origin = position
	if (object):
		current_path.pass_point(object, true)
	current_path.curve.add_point(Vector3.ZERO)
	trails.append(current_path)

func _on_update_trail(position : Vector3, object = null):
	var curve = current_path.curve
	if (object != null):
		current_path.pass_point(object, false)
	if (to_global(position).distance_to(to_global(curve.get_point_position(curve.get_point_count() - 1))) < (distance / 2)):
		curve.remove_point(curve.get_point_count() - 1)
	curve.add_point(current_path.to_local(position))
	current_path.update()


#take the array of ants that are trailing player character
#[iacus + any followers]
#and assign them to the last laid path
#return an array representing the status of the operation
#untouched if unsuccessful, just the player character
#if successful
func _on_add_pather(items : Array) -> void:
	#if there are no followers, or no trails, return
	if (items.size() <= 1 || trails.size() == 0):
		emit_signal("report_remaining_pathers", items)
		return
	
	#remove the player character from the array, we don't want to assign them
	#to the path
	var front = items.pop_front()
	
	for item in items:
		item.start_pathing(current_path.pathfollow)
	
	emit_signal("report_remaining_pathers", [front])
