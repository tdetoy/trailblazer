extends PathFollow

signal hit_endpoint(point_type)

var startpoint = null
var endpoint = null

var length := 0.0
var current_offset := 0.0
var ascending := true

func update():
	rotation_mode = PathFollow.ROTATION_NONE
	var points = get_parent().curve.get_baked_points()
	
	length = 0.0
	
	for i in range(1, points.size()):
		length += points[i].distance_to(points[i - 1])
	

func update_offset(value):
	prints(current_offset, length, ascending)
	current_offset += value if ascending else -value
	if (current_offset >= length - 3 && ascending):
		current_offset = length
		emit_signal("hit_endpoint", endpoint)
		ascending = false
	elif (current_offset <= 0.0 && !ascending):
		current_offset = 0.0
		emit_signal("hit_endpoint", startpoint)
		ascending = true
	offset = current_offset
