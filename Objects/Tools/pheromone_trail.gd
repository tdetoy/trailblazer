extends Path

onready var pathfollow = $AntPathPosition

func pass_point(point = null, is_start = true):
	if (is_start):
		pathfollow.startpoint = point
	else:
		pathfollow.endpoint = point

func update():
	pathfollow.update()
