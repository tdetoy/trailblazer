extends Spatial

var ant_template = preload("res://Objects/Worker/WorkerAntArea.tscn")

onready var ant_settings = UnitSettings.new()

class UnitSettings:
	var min_speed := 2.0
	var speed := 20.0
	var deceleration := 5.0
	var perception_radius := 7.5
	var avoidance_radius := 5.0
	var max_steer_force := 9.0
	var follow_distance := 7.0
	var appearance_scale := 3.0
	var avoid_coll_weight := 1.0

func _on_summon_ant() -> void:
	var new_ant = ant_template.instance()
	add_child(new_ant)
	new_ant.init(self, ant_settings)
	new_ant.transform.origin = Vector3(randf() * 10.0, 1.0, randf() * 10.0)

func get_settings() -> UnitSettings:
	return ant_settings

func set_settings(settings : UnitSettings) -> void:
	ant_settings = settings
