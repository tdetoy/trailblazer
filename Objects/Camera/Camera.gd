extends Spatial

export (float) var rotate_speed := 5.0
export (float) var zoom_speed := 3000.0

var match_rot = false

func _process(delta):
	var target_zoom = clamp($Camera.size + ((int(Input.is_action_just_released("zoom_camera_out")) - int(Input.is_action_just_released("zoom_camera_in"))) * zoom_speed), 100.0, 200.1)
	var rotate = Input.get_action_strength("rotate_camera_right") - Input.get_action_strength("rotate_camera_left")
	rotate_y(rotate * rotate_speed * delta)
	$Camera.size = lerp($Camera.size, target_zoom, delta * 5)

func _compensate_rotation(rot_value):
	rotate_y(rot_value)
