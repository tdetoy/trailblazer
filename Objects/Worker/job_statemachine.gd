extends Node

signal job_complete

enum STATE { IDLE, GATHER, PROVIDE }

var state_key = {"Gather" : STATE.GATHER,
				 "Provide" : STATE.PROVIDE}

var state = STATE.IDLE

var body = null
var holding = false

func initialize(_body):
	body = _body

func process_state(arg):
	state = determine_states(arg)
	
	match state:
		STATE.IDLE:
			pass
		STATE.GATHER:
			harvest(arg)
		STATE.PROVIDE:
			provide(arg)
	
	emit_signal("job_complete")

func determine_states(location):
	for action in location.possible_actions:
		if (state_key.keys().has(action)):
			return state_key[action]
	return STATE.IDLE

func harvest(location):
	location.decrement_uses()
	var item = load("res://Objects/Food/Berries/pebble1.tscn").instance()
	item.scale *= 2.0
	body.place_in_mouth(item)
	holding = true

func provide(recipient):
	if (body.remove_from_mouth()):
		recipient.receive_food()
