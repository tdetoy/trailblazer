extends Area

#################################################
# FOR ANTS TO WORK PROPERLY:                    #
# there need to be 3 things-                    #
# the pheromone trail container                 #
# ^ ("PheromoneTrail.tscn")                     #
# the 3 ant containers                          #
# ^ ("AntContainers.tscn")                      #
# nodepaths for both in Iacus's path variables  #
#################################################
# ALSO!                                         #
# any terrain the ants collide with             #
# needs to be on layer 3                        #
# otherwise they will not align themselves      #
# or place themselves on a surface              #
# (Layer 1 is for Iacus and 2 is for the areas  #
# the ants scan on for other ants)              #
#################################################

#debug signals
signal processing_ant(value)

#operation signals
signal child_left
signal rot_update(value)

enum STATE { WANDER, FOLLOW, PATH, PREPATH, POSTPATH }

onready var job_sm = $AntJobStateMachine

var follow_parent
var wander_parent
var path_parent

var settings
var state = STATE.WANDER

var held_object = null
var interest_point = null
var target_object = null
var tracked_entities := []
var entities_too_close := []
var offset_dist := 0.0

var cached_xform := Transform.IDENTITY
var target := Vector3.INF


func _ready() -> void:
	cached_xform = transform
	job_sm.initialize(self)
#	prints("connecting 'processing_ant':", connect("processing_ant", get_tree().get_root().get_node("Spatial/DebugInfo"), "_update_processing"))


func init(container_parent : Spatial, _settings = null, _target : Vector3 = Vector3.INF) -> void:
	target = _target
	settings = _settings
	
	transform.origin = cached_xform.origin
	transform.basis.z = cached_xform.basis.z
	
	if (settings == null):
		print("cannot invoke new ant with no settings")
		queue_free()
		return
	
	$"worker-ant".transform.basis.x *= settings.appearance_scale
	$"worker-ant".transform.basis.y *= settings.appearance_scale
	$"worker-ant".transform.basis.z *= settings.appearance_scale
	
	$PersonalBubble/CollisionShape.shape.radius = settings.avoidance_radius
	$DetectionRadius/CollisionShape.shape.radius = settings.perception_radius
	
	follow_parent = container_parent.get_node("FollowingAnts")
	wander_parent = container_parent.get_node("WanderingAnts")
	path_parent = container_parent.get_node("PathingAnts")

func _process(delta : float) -> void:
	var velocity = Vector3.ZERO
	
	if ((state == STATE.PATH || state == STATE.PREPATH) && $PersonalBubble.monitoring):
		$PersonalBubble.monitoring = false
		$DetectionRadius.monitoring = false
	elif (!$PersonalBubble.monitoring):
		$PersonalBubble.monitoring = true
		$DetectionRadius.monitoring = true
	
	match state:
		STATE.WANDER:
			velocity = update_unit_wander(delta)
		STATE.FOLLOW:
			velocity = update_unit_follow(delta)
		STATE.PATH:
			velocity = update_unit_path(delta)
		STATE.PREPATH, STATE.POSTPATH:
			velocity = update_unit_prepath(delta)
	
	if (velocity.length_squared() > 1.0):
		velocity = velocity.normalized() * min(velocity.length(), settings.speed)
	
	if (velocity != Vector3.ZERO):
		$"worker-ant/AnimationPlayer".play("WalkCycle") if !job_sm.holding else $"worker-ant/AnimationPlayer".play("WalkCycleHolding")
		match state:
			STATE.WANDER:
				if (velocity.length_squared() > 1.0):
					transform.origin += velocity * delta
			STATE.FOLLOW, STATE.PATH, STATE.PREPATH:
				look_at(to_global(velocity.normalized()), transform.basis.y)
				transform.origin -= transform.basis.z * delta * velocity.length()
	else:
		$"worker-ant/AnimationPlayer".stop()
	
	emit_signal("rot_update", cached_xform.basis.z.angle_to(transform.basis.z))
	
	#cast ray at ground from above ant, get normal and ypos of
	#contact. then, put ant there and rebuild transform so it 
	#sits on slope properly
	var y_info = get_ground_info()

	if (y_info.size() > 0):
		transform.origin.y = y_info[0]
		var x_transform = transform.basis.z.cross(y_info[1])
		var z_transform = x_transform.cross(y_info[1])
		z_transform *= -1
		x_transform = z_transform.cross(y_info[1])
		x_transform *= -1
		transform.basis = Basis(x_transform.normalized(), y_info[1], z_transform.normalized())

	cached_xform = global_transform

func update_unit_wander(_delta : float) -> Vector3:
	var vel = Vector3.ZERO
	var accel = Vector3.ZERO
	
	if (entities_too_close.size() > 0):
		var avoid_coll_dir = _find_open_space()
		var avoid_force = steer_towards(avoid_coll_dir, vel) * settings.avoid_coll_weight
		accel += avoid_force
	
	if (target == Vector3.INF && !vel.is_equal_approx(Vector3.ZERO)):
		vel.x -= min(settings.deceleration, vel.x) if vel.x > 0 else max(-settings.deceleration, vel.x)
		vel.z -= min(settings.deceleration, vel.z) if vel.z > 0 else max(-settings.deceleration, vel.z)
	
	vel += accel
	
	return vel

func update_unit_follow(_delta : float) -> Vector3:
	var vel = Vector3.ZERO
	
	if (target_object == null || !is_instance_valid(target_object)):
		target_object = null
		state = STATE.WANDER
		return Vector3.ZERO
	
	if (target_object.is_in_group("TrailConnection")):
		target = to_local(target_object.get_back_connection())
	else:
		target = target_object.transform.origin
	
	if (to_global(target).distance_to(global_transform.origin) < 0.5):
		return Vector3.ZERO
	
	var accel = Vector3.ZERO
	accel = steer_towards(target, vel)
	
	vel += accel
	
	return vel

func update_unit_path(delta : float) -> Vector3:
	target_object.update_offset(delta * settings.speed)
	
	return to_local(target_object.global_transform.origin)

func update_unit_prepath(_delta : float) -> Vector3:
	var vel = Vector3.ZERO
	
	prints("distance to:", target_object.global_transform.origin.distance_to(global_transform.origin))
	
	if (target_object.global_transform.origin.distance_squared_to(global_transform.origin) < 4.0):
		print("close enough")
		if (interest_point != null):
			job_sm.process_state(interest_point)
		state = STATE.PATH if state == STATE.PREPATH else STATE.FOLLOW
		return vel
	
	vel += steer_towards(to_local(target_object.global_transform.origin), vel)
	prints("vel:", vel)
	return vel

func steer_towards(vec : Vector3, vel : Vector3) -> Vector3:
	var v = vec.normalized() * settings.speed - vel
	if (v.length() > settings.max_steer_force):
		v = v.normalized() * settings.max_steer_force
	return v

#run through each item in personal bubble
#find distance from transform.origin
#find average point (basically, centerpoint of all nearby neighbors)
#find x/z weight (+z + -z and +x + -x, find whether nx and nz have more positive or negative values)
#if both are 0, do nothing to average
#if one is more pos/neg than the other, constrain that dimension to the opposite of whatever pos/neg is found
#if they are the same, flip both dimensions

func _find_open_space() -> Vector3:
	var average = Vector3.ZERO
	var nx = 0
	var nz = 0
	for entity in entities_too_close:
		var location = entity.transform.origin - transform.origin
		average += Vector3(location.x, 0.0, location.z)
		if (location.x > 0):
			nx += 1
		elif (location.x < 0):
			nx -= 1
		if (location.z > 0):
			nz += 1
		elif (location.z < 0):
			nz -= 1
	
	average = average.normalized() * min(average.length(), $PersonalBubble/CollisionShape.shape.radius)
	
	if (nx == 0 && nz == 0):
		return average
	if (abs(nx) == abs(nz)):
		average *= -1
	elif (abs(nx) > abs(nz)):
		average.x *= -1
	elif (abs(nz) > abs(nx)):
		average.z *= -1
	
	return average

func _on_PersonalBubble_area_entered(area : Area) -> void:
	if (area == self):
		return
	if (!entities_too_close.has(area)):
		emit_signal("processing_ant", 1)
		entities_too_close.append(area)

func _on_DetectionRadius_area_entered(area : Area) -> void:
	if (area == self):
		return
	if (!tracked_entities.has(area)):
		tracked_entities.append(area)

func _on_DetectionRadius_body_entered(body) -> void:
	if (!tracked_entities.has(body)):
		tracked_entities.append(body)

func _on_PersonalBubble_area_exited(area : Area) -> void:
	emit_signal("processing_ant", -1)
	entities_too_close.erase(area)

func _on_DetectionRadius_area_exited(area : Area) -> void:
	tracked_entities.erase(area)

func _on_DetectionRadius_body_exited(body) -> void:
	tracked_entities.erase(body)

func _on_WorkerAnt_body_entered(body) -> void:
	if (body.name == "Iacus" && state != STATE.FOLLOW):
		if (!body.follower_ready):
			return
		target_object = body.add_to_line(self)
		get_parent().call_deferred("remove_child", self)
		emit_signal("child_left")
		follow_parent.call_deferred("add_child", self)
		state = STATE.FOLLOW

func _on_hit_endpoint(point):
	interest_point = point
	state = STATE.PREPATH

func create_connection(connection) -> void:
	add_child(connection)
	connection.set_connection_length(settings.follow_distance)
	prints("connecting 'child_left':", connect("child_left", connection, "queue_free"))
	prints("connecting 'rot_update", connect("rot_update", connection, "_on_rot_update"))

func start_pathing(follow) -> void:
	target_object = follow
#	transform.origin = Vector3.ZERO
	state = STATE.PREPATH
	get_parent().remove_child(self)
	emit_signal("child_left")
	path_parent.add_child(self)
	prints("connecting 'hit_endpoint':", follow.connect("hit_endpoint", self, "_on_hit_endpoint"))
	prints("connecting 'child_left':", connect("child_left", target_object, "queue_free"))

func get_ground_info() -> Array:
	if (!$RayCast.get_collider()):
		return []
	var info = $RayCast.get_collider()
	return [$RayCast.get_collision_point().y, $RayCast.get_collision_normal()]


func place_in_mouth(object):
	$"worker-ant/AnimationPlayer".play("MandibleOpen")
	$"worker-ant/Worker_Ant_Arm/Skeleton/Worker_Ant/HoldPosition".add_child(object)
	held_object = object

func remove_from_mouth():
	if (!held_object):
		return false
	$"worker-ant/Worker_Ant_Arm/Skeleton/Worker_Ant/HoldPosition".remove_child(held_object)
	held_object.queue_free()
	held_object = null
	return true
